import "./styles/styles.css";
import StringValidator from "./stringValidator.js";

const slider = document.getElementById("income");
const output = document.getElementById("incomeValue");
const submitButton = document.getElementById("submit");
const validationMessageContainer = document.getElementById(
  "validationMessageContainer"
);
const email = document.getElementById("email");
const firstName = document.getElementById("firstName");
const pass = document.getElementById("pass");
const confirmpass = document.getElementById("confirmpass");
const validator = new StringValidator();

output.innerHTML = slider.value;
slider.oninput = function () {
  output.innerHTML = this.value;
};

submitButton.onclick = () => {
  validationMessageContainer.innerHTML = "";
  validationMessageContainer.style.display = "none";

  if (!validator.isEmail(email.value)) {
    validationMessageContainer.style.display = "flex";
    addValidationMessage(validationMessageContainer, "Email is not valid!");
  }
  if (validator.isEmpty(firstName.value)) {
    validationMessageContainer.style.display = "flex";
    addValidationMessage(validationMessageContainer, "FirstName is required!");
  } else if (!validator.containsOnlyLetters(firstName.value)) {
    validationMessageContainer.style.display = "flex";
    addValidationMessage(validationMessageContainer, "FirstName is not valid!");
  }
  if (validator.isEmpty(pass.value)) {
    validationMessageContainer.style.display = "flex";
    addValidationMessage(validationMessageContainer, "Password is required!");
  }
  if (validator.isEmpty(confirmpass.value)) {
    validationMessageContainer.style.display = "flex";
    addValidationMessage(
      validationMessageContainer,
      "Password confirmation is required!"
    );
  }
};

function addValidationMessage(parent, message) {
  let label = document.createElement("label");
  label.classList.add("validation-error-message");
  label.innerHTML = message;
  parent.appendChild(label);
}

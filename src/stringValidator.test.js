import StringValidator from "./stringValidator";

const validator = new StringValidator();

test("isEmailTest", () => {
  let valid = validator.isEmail("valid@valid.com");
  let invalid = validator.isEmail("invalid");
  expect(valid).toBe(true);
  expect(invalid).toBe(false);
});

test("isDateTest", () => {
  let valid = validator.isDate("01/01/2022");
  let invalid = validator.isDate("invalid");
  expect(valid).toBe(true);
  expect(invalid).toBe(false);
});

test("isEmptyTest", () => {
  let valid = validator.isEmpty("");
  let invalid = validator.isEmpty("invalid");
  expect(valid).toBe(true);
  expect(invalid).toBe(false);
});

test("containsOnlyLettersTest", () => {
  let valid = validator.containsOnlyLetters("valid");
  let invalid = validator.containsOnlyLetters("invalid3333");
  expect(valid).toBe(true);
  expect(invalid).toBe(false);
});

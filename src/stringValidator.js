const emailRegex = new RegExp(
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);
const dateRegex = new RegExp(
  /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/
);
const lettersRegex = new RegExp(/^[a-zA-Z]+$/);

export default class StringValidator {
  constructor() {}

  isEmail(potentialEmail) {
    return emailRegex.test(potentialEmail.toLowerCase());
  }

  isDate(potentialDate) {
    return dateRegex.test(potentialDate);
  }

  containsOnlyLetters(string) {
    return lettersRegex.test(string);
  }

  isEmpty(expression) {
    return expression === "";
  }
}
